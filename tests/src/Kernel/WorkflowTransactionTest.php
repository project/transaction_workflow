<?php

namespace Drupal\Tests\transaction_workflow\Kernel;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\transaction\Kernel\KernelTransactionTestBase;
use Drupal\transaction\Entity\TransactionType;

/**
 * Tests the workflow transactor.
 *
 * @group transaction_workflow
 */
class WorkflowTransactionTest extends KernelTransactionTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'options',
    'transaction_workflow',
  ];

  /**
   * {@inheritdoc}
   */
  protected function prepareTransactionType() {
    $this->transactionType = TransactionType::create([
      'id' => 'test_workflow',
      'label' => 'Test workflow',
      'target_entity_type' => 'entity_test',
      'transactor' => [
        'id' => 'transaction_workflow',
        'settings' => [
          'last_transaction' => 'field_last_transaction',
          'log_message' => 'field_log_message',
          'state' => 'field_state',
          'target_state' => 'field_state',
          'transitions_' => 'first',
          'transitions_first' => 'second',
          'transitions_second' => 'first,third',
          'transitions_third' => '',
        ],
      ],
      'third_party_settings' => [
        'transaction_workflow' => [
          'states' => [
            'first' => 'First',
            'second' => 'Second',
            'third' => 'Third',
          ],
        ],
      ],
    ]);
    $this->transactionType->save();

    $this->prepareTransactionStateField();
    $this->prepareTargetEntityStateField();

    // Adds the test log message field.
    $this->addTransactionLogMessageField();
  }

  /**
   * Creates the state field in the transaction entity type.
   */
  protected function prepareTransactionStateField() {
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_state',
      'type' => 'list_string',
      'entity_type' => 'transaction',
    ]);
    $field_storage->save();

    FieldConfig::create([
      'label' => 'State',
      'field_storage' => $field_storage,
      'bundle' => $this->transactionType->id(),
    ])->save();
  }

  /**
   * Creates the state field in the target entity type.
   */
  protected function prepareTargetEntityStateField() {
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_state',
      'type' => 'list_string',
      'entity_type' => 'entity_test',
    ]);
    $field_storage->save();

    FieldConfig::create([
      'label' => 'State',
      'field_storage' => $field_storage,
      'bundle' => 'entity_test',
    ])->save();
  }

  /**
   * Tests workflow transaction creation.
   */
  public function testWorkflowTransactionCreation() {
    $transaction = $this->transaction;
    $transaction->set('field_state', 'first');

    // Checks status for new transaction.
    $this->assertEquals('Transition to First state (pending)', strip_tags($transaction->getDescription()));
    $this->assertEquals([$this->logMessage], $transaction->getDetails());
    $transaction->set('field_state', 'second');
    $this->assertEquals('Transition to Second state (pending)', strip_tags($transaction->getDescription(TRUE)));
  }

  /**
   * Tests workflow transaction execution.
   */
  public function testWorkflowTransactionExecution() {
    $first_transaction = $this->transaction;
    $first_transaction->set('field_state', 'first');

    $this->assertTrue($first_transaction->execute());
    // Checks the transaction status after its execution.
    $this->assertGreaterThan(0, $first_transaction->getResultCode());
    $this->assertEquals('Transaction executed successfully.', $first_transaction->getResultMessage());
    $this->assertEquals('Transition to First state', strip_tags($first_transaction->getDescription()));
    $this->assertEquals($first_transaction->id(), $this->targetEntity->get('field_last_transaction')->target_id);
    // Checks the workflow state.
    $this->assertEquals('first', $first_transaction->get('field_state')->value);
    $this->assertEquals('first', $this->targetEntity->get('field_state')->value);

    // Tries an illegal transition.
    $this->prepareTransaction();
    $illegal_transaction = $this->transaction;
    // Transition from the first state to the third state is no allowed.
    $illegal_transaction->set('field_state', 'third');
    $this->assertFalse($illegal_transaction->execute());
    $this->assertLessThan(0, $illegal_transaction->getResultCode());
    $this->assertEquals('Illegal workflow transition.', $illegal_transaction->getResultMessage());

    // Checks valid state transition.
    $this->prepareTransaction();
    $second_transaction = $this->transaction;
    $second_transaction->set('field_state', 'second');
    $this->assertTrue($second_transaction->execute());
    // Checks the new workflow status.
    $this->assertEquals('Transition to Second state', strip_tags($second_transaction->getDescription()));
    $this->assertEquals('second', $second_transaction->get('field_state')->value);
    $this->assertEquals('second', $this->targetEntity->get('field_state')->value);
    $this->assertEquals($second_transaction->id(), $this->targetEntity->get('field_last_transaction')->target_id);
  }

}
